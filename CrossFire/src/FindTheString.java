import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;


public class FindTheString {
	
	String path;
	String search_string;
	String replace_string = "";
	
	public FindTheString(String path_in,String search_string){
		path = path_in;
		this.search_string = search_string;
	}
	
	public FindTheString(String path_in,String search_string,String replace_string){
		path = path_in;
		this.search_string = search_string;
		this.replace_string = replace_string;
	}
	
	@SuppressWarnings("resource")
	public String showResult() {
		
		String text = "";
		
		try {
			
			File f = new File(path);
			
			if(f.length()>0){
				text = new Scanner(new File(path), "UTF-8" ).useDelimiter("\\A").next();
				int pos = text.indexOf(this.search_string);
				//int pos_end = pos+this.search_string.length();
				
				
					if(pos >= 0){
						
						//replace string
						if(!this.replace_string.equals("")){
							text = Pattern.compile("\\b"+search_string+"\\b").matcher(text).replaceFirst(replace_string);
							replace_string = "";
							
							//write file
						    BufferedWriter writer = null;
						    try
						    {
						        writer = new BufferedWriter( new FileWriter(path));
						        writer.write(text);
						        return "replaced -> "+path;
						    }
						    catch ( IOException ex){ex.printStackTrace();}
						    finally{
						        try
						        {
						            if ( writer != null)
						            writer.close( );
						        }
						        catch ( IOException ex){ex.printStackTrace();}
						    }
							
						}
						
						return path;
					}else{
						return "NO";
					}
			}
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			//JOptionPane.showMessageDialog(new MainFire(), "File not found");
		}
		return "NO";	
	}
	
}
