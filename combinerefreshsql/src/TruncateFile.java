import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;


public class TruncateFile {
	
	String path;
	
	public TruncateFile(String path_in){
		path = path_in;
	}
	
	@SuppressWarnings("resource")
	public String showResult() {
		
		String text = "";
		
		try {
			File f = new File(path);
			System.out.println("FILE_LENGTH= "+f.length());
			if(f.length()>0){
				text = new Scanner( new File(path), "UTF-8" ).useDelimiter("\\A").next();
				//set start and end key words
				int pos = text.indexOf("procedure");
				int pos_end = text.lastIndexOf("end")+3;
				//replace as instead of is
				text = Pattern.compile("\\bis\\b").matcher(text).replaceFirst("as");

					if(pos > -1){
						return (String) text.subSequence(pos, pos_end);
					}else{
						return text;
					}
			}
			//text= text+"\n";
			//text = text+new Scanner( new File(path), "UTF-8" ).useDelimiter("\\A").next();
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			JOptionPane.showMessageDialog(new ReadFileName(), "File not found");
		}
		return "";
		
		
	}
}
