import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 *
 * @author Kenji Chen
 */
public class ReadFileName extends javax.swing.JFrame {

	
    /**
     * Creates new form ReadFileName
     */
    public ReadFileName() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {
    	
    	this.setTitle("Sync.sql Combine!!");
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextField1.setText("");
        jTextField1.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                jTextField1.setText("");
            }
        });


        jButton1.setText("Create Refresh SQL file");
        jButton1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				try {
					String text = new Scanner( new File(jTextField1.getText()+"/XYZ.sql"), "UTF-8" ).useDelimiter("\\A").next();
					text= text+"\n";
					text = text+new Scanner( new File(jTextField1.getText()+"/DEF.sql"), "UTF-8" ).useDelimiter("\\A").next();
					System.out.println(text);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				
				try{
				File folder = new File(jTextField1.getText());
				File[] listOfFiles = folder.listFiles();
				ArrayList<String> names = new ArrayList<String>();
				ArrayList<String> procedure_names = new ArrayList<String>();
				String feedback = "";
				String eol = System.getProperty("line.separator");  

				    for (int i = 0; i < listOfFiles.length; i++) {
				      if (listOfFiles[i].isFile()) {
				        names.add(listOfFiles[i].getName());
				      }
				    }
				    
				    for (int i=0; i<names.size(); i++){
				    	String path = null;
				    	path = jTextField1.getText()+"\\"+names.get(i);
				    	System.out.println(path);
				    	TruncateFile tf = new TruncateFile(path);
				    	feedback += tf.showResult()+";"+eol+eol+eol;
				    	
				    	//get each procedure name
				    	String[] pro_name = tf.showResult().split("\\s+");
				    	procedure_names.add(pro_name[1]);
				    }
				    
				    feedback += "procedure refresh as"+eol+"BEGIN"+eol+eol;
				    for (int j=0; j<procedure_names.size(); j++){
				    	feedback += "begin"+eol
				    				+"dbms_output.put_line('"+procedure_names.get(j)+"');"+eol
				    				+procedure_names.get(j)+";"+eol
				    				+"exception when others then"+eol
				    				+"dbms_output.put_line(sqlerrm);"+eol
				    				+"end;"+eol+eol;
				    }
				    feedback += "END refresh;"+eol+eol
				    			+"BEGIN"+eol
				    			+"null;"+eol
				    			+"END FWEB_REFRESH_LOV;"+eol
				    			+"/";
				    
				    //write file
				    BufferedWriter writer = null;
				    try
				    {
				        writer = new BufferedWriter( new FileWriter(jTextField1.getText()+"/_fweb_refresh_lov_b.sql"));
				        writer.write(feedback);
				        JOptionPane.showMessageDialog(ReadFileName.this, "Succeed!!!");

				    }
				    catch ( IOException ex)
				    {
				    }
				    finally
				    {
				        try
				        {
				            if ( writer != null)
				            writer.close( );
				        }
				        catch ( IOException ex)
				        {
				        }
				    }
				    
				    
				}
				catch(Exception ex){
					ex.printStackTrace();
					JOptionPane.showMessageDialog(ReadFileName.this, "Wrong Path~~");
				}
				    
				
			}
		});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(jTextField1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ReadFileName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ReadFileName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ReadFileName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ReadFileName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ReadFileName().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify
    private javax.swing.JButton jButton1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration
}
